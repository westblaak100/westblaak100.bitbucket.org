<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
<meta name="viewport" content="width=device-width">
<meta name="description" content="Brandaris, a strong and sleek CSS-only framework.">
<meta name="author" content="Maarten Brakkee">

<title>Spacing · Brandaris</title>

<!-- Brandaris core CSS -->
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Inconsolata|Lato|Noto+Serif:400,400i,700&display=swap">
<link href="/brandaris/assets/styles/brandaris.min.css" rel="stylesheet">
<link href="/brandaris/assets/styles/brandaris.docs.min.css" rel="stylesheet">

<!-- Favicons -->
<link rel="apple-touch-icon" href="/brandaris/assets/images/apple-touch-icon.png">
<link rel="icon" href="/brandaris/favicon.ico">

<!-- Brandaris Open Graph -->
<meta property="og:locale" content="en_UK">
<meta property="og:type" content="website">
<meta property="og:site_name" content="Brandaris: CSS-only framework">
<meta property="og:title" content="Spacing &middot; Brandaris">
<meta property="og:description" content="Brandaris, a strong and sleek CSS-only framework.">
<meta property="og:url" content="http://maartenbrakkee.bitbucket.org/Brandaris/">
<meta property="og:image" content="http://maartenbrakkee.bitbucket.org/Brandaris/assets/images/repo.jpg">

<script src="/brandaris/assets/scripts/brandaris.header.min.js" async></script>

</head>

<body>
  <div class="bg-blue py-2 mb-2">
    <div class="container">
      <div class="row middle">
        <div class="col-md-9 clr-white">
          
          <h1>Base</h1>
          <p class="lead">Global styles, such as typography, colors, grid, etc.</p>
          
        </div>
        <div class="col-md-3">
          <div id="search-container" class="search">
            <input type="text" id="search-input" placeholder="search..." aria-label="Search">
            <div class="results hide">
              <ul id="results-container"></ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <main class="content container">
    <div class="row">
      <div class="col-md-9 col-lg-8 last-lg">
        <h2>Spacing</h2>
        <h3 id="how-it-works">How it works</h3>

<p>Assign responsive-friendly <code class="highlighter-rouge">margin</code> or <code class="highlighter-rouge">padding</code> values to an element or a subset of its sides with shorthand classes. Includes support for individual properties, all properties, and vertical and horizontal properties.</p>

<h3 id="notation">Notation</h3>

<p>Spacing utilities that apply to all breakpoints, from <code class="highlighter-rouge">xs</code> to <code class="highlighter-rouge">xl</code>, have no breakpoint abbreviation in them. This is because those classes are applied from <code class="highlighter-rouge">min-width: 0</code> and up, and thus are not bound by a media query. The remaining breakpoints, however, do include a breakpoint abbreviation.</p>

<p>The classes are named using the format <code class="highlighter-rouge">{property}{sides}-{size}</code> for <code class="highlighter-rouge">xs</code> and <code class="highlighter-rouge">{property}{sides}-{breakpoint}-{size}</code> for <code class="highlighter-rouge">sm</code>, <code class="highlighter-rouge">md</code>, <code class="highlighter-rouge">lg</code>, and <code class="highlighter-rouge">xl</code>.</p>

<p>Where <em>property</em> is one of:</p>

<ul>
  <li><code class="highlighter-rouge">m</code> - for classes that set <code class="highlighter-rouge">margin</code></li>
  <li><code class="highlighter-rouge">p</code> - for classes that set <code class="highlighter-rouge">padding</code></li>
</ul>

<p>Where <em>sides</em> is one of:</p>

<ul>
  <li><code class="highlighter-rouge">t</code> - for classes that set <code class="highlighter-rouge">margin-top</code> or <code class="highlighter-rouge">padding-top</code></li>
  <li><code class="highlighter-rouge">b</code> - for classes that set <code class="highlighter-rouge">margin-bottom</code> or <code class="highlighter-rouge">padding-bottom</code></li>
  <li><code class="highlighter-rouge">l</code> - for classes that set <code class="highlighter-rouge">margin-left</code> or <code class="highlighter-rouge">padding-left</code></li>
  <li><code class="highlighter-rouge">r</code> - for classes that set <code class="highlighter-rouge">margin-right</code> or <code class="highlighter-rouge">padding-right</code></li>
  <li><code class="highlighter-rouge">x</code> - for classes that set both <code class="highlighter-rouge">*-left</code> and <code class="highlighter-rouge">*-right</code></li>
  <li><code class="highlighter-rouge">y</code> - for classes that set both <code class="highlighter-rouge">*-top</code> and <code class="highlighter-rouge">*-bottom</code></li>
  <li>blank - for classes that set a <code class="highlighter-rouge">margin</code> or <code class="highlighter-rouge">padding</code> on all 4 sides of the element</li>
</ul>

<p>Where <em>size</em> is one of:</p>

<ul>
  <li><code class="highlighter-rouge">0</code> - for classes that eliminate the <code class="highlighter-rouge">margin</code> or <code class="highlighter-rouge">padding</code> by setting it to <code class="highlighter-rouge">0</code></li>
  <li><code class="highlighter-rouge">1</code> - (by default) for classes that set the <code class="highlighter-rouge">margin</code> or <code class="highlighter-rouge">padding</code> to <code class="highlighter-rouge">$spacer * .25</code></li>
  <li><code class="highlighter-rouge">2</code> - (by default) for classes that set the <code class="highlighter-rouge">margin</code> or <code class="highlighter-rouge">padding</code> to <code class="highlighter-rouge">$spacer * .5</code></li>
  <li><code class="highlighter-rouge">3</code> - (by default) for classes that set the <code class="highlighter-rouge">margin</code> or <code class="highlighter-rouge">padding</code> to <code class="highlighter-rouge">$spacer</code></li>
  <li><code class="highlighter-rouge">4</code> - (by default) for classes that set the <code class="highlighter-rouge">margin</code> or <code class="highlighter-rouge">padding</code> to <code class="highlighter-rouge">$spacer * 1.5</code></li>
  <li><code class="highlighter-rouge">5</code> - (by default) for classes that set the <code class="highlighter-rouge">margin</code> or <code class="highlighter-rouge">padding</code> to <code class="highlighter-rouge">$spacer * 3</code></li>
  <li><code class="highlighter-rouge">auto</code> - for classes that set the <code class="highlighter-rouge">margin</code> to auto</li>
</ul>

<p>(You can add more sizes by adding entries to the <code class="highlighter-rouge">$spacers</code> Sass map variable.)</p>

<h3 id="examples">Examples</h3>

<p>Here are some representative examples of these classes:</p>

<figure class="highlight"><pre><code class="language-scss" data-lang="scss"><span class="nc">.mt-0</span> <span class="p">{</span>
  <span class="nl">margin-top</span><span class="p">:</span> <span class="m">0</span> <span class="o">!</span><span class="n">important</span><span class="p">;</span>
<span class="p">}</span>

<span class="nc">.ml-1</span> <span class="p">{</span>
  <span class="nl">margin-left</span><span class="p">:</span> <span class="p">(</span><span class="nv">$spacer</span> <span class="o">*</span> <span class="mi">.25</span><span class="p">)</span> <span class="o">!</span><span class="n">important</span><span class="p">;</span>
<span class="p">}</span>

<span class="nc">.px-2</span> <span class="p">{</span>
  <span class="nl">padding-left</span><span class="p">:</span> <span class="p">(</span><span class="nv">$spacer</span> <span class="o">*</span> <span class="mi">.5</span><span class="p">)</span> <span class="o">!</span><span class="n">important</span><span class="p">;</span>
  <span class="nl">padding-right</span><span class="p">:</span> <span class="p">(</span><span class="nv">$spacer</span> <span class="o">*</span> <span class="mi">.5</span><span class="p">)</span> <span class="o">!</span><span class="n">important</span><span class="p">;</span>
<span class="p">}</span>

<span class="nc">.p-3</span> <span class="p">{</span>
  <span class="nl">padding</span><span class="p">:</span> <span class="nv">$spacer</span> <span class="o">!</span><span class="n">important</span><span class="p">;</span>
<span class="p">}</span></code></pre></figure>

<h4 id="horizontal-centering">Horizontal centering</h4>

<p>Additionally, Bootstrap also includes an <code class="highlighter-rouge">.mx-auto</code> class for horizontally centering fixed-width block level content—that is, content that has <code class="highlighter-rouge">display: block</code> and a <code class="highlighter-rouge">width</code> set—by setting the horizontal margins to <code class="highlighter-rouge">auto</code>.</p>

<div class="bd-example">
  <div class="mx-auto" style="width: 200px; background-color: rgba(86,61,124,.15);">
    Centered element
  </div>
</div>

<figure class="highlight"><pre><code class="language-html" data-lang="html"><span class="nt">&lt;div</span> <span class="na">class=</span><span class="s">"mx-auto"</span> <span class="na">style=</span><span class="s">"width: 200px;"</span><span class="nt">&gt;</span>
  Centered element
<span class="nt">&lt;/div&gt;</span></code></pre></figure>


      </div>
      <div class="col-md-3 col-lg-2 last-lg d-none d-md-block">
        <div class="tocbot-wrap">
          <span class="clr-gray">On this page</span>
          <div class="tocbot pt-2"></div>
        </div>
      </div>
      <div class="col-lg-2">
        <nav class="docs-nav">
          <ul class="menu">
        <li class="active  branch  open"><a href="/brandaris/docs/base/">Base</a><ul class="menu">
        <li class="leaf"><a href="/brandaris/docs/base/colors/">Colors</a></li><li class="leaf"><a href="/brandaris/docs/base/display/">Display</a></li><li class="leaf"><a href="/brandaris/docs/base/grid/">Grid</a></li><li class="leaf"><a href="/brandaris/docs/base/images/">Images</a></li><li class="selected  leaf"><a href="/brandaris/docs/base/spacing/">Spacing</a></li><li class="leaf"><a href="/brandaris/docs/base/typography/">Typography</a></li>
    </ul></li><li class="branch  open"><a href="/brandaris/docs/components/">Components</a><ul class="menu">
        <li class="leaf"><a href="/brandaris/docs/components/badge/">Badges and circles</a></li><li class="leaf"><a href="/brandaris/docs/components/box/">Boxes</a></li><li class="leaf"><a href="/brandaris/docs/components/buttons/">Buttons</a></li><li class="leaf"><a href="/brandaris/docs/components/charts/">Charts</a></li><li class="leaf"><a href="/brandaris/docs/components/dropdowns/">Dropdowns</a></li><li class="leaf"><a href="/brandaris/docs/components/forms/">Forms</a></li><li class="leaf"><a href="/brandaris/docs/components/icons/">Icons</a></li><li class="leaf"><a href="/brandaris/docs/components/notifications/">Notifications</a></li><li class="leaf"><a href="/brandaris/docs/components/spinner/">Spinner</a></li><li class="leaf"><a href="/brandaris/docs/components/tables/">Tables</a></li><li class="leaf"><a href="/brandaris/docs/components/triangles/">Triangles</a></li>
    </ul></li><li class="branch  open"><a href="/brandaris/docs/interaction/">Interaction</a><ul class="menu">
        <li class="leaf"><a href="/brandaris/docs/interaction/on-change/">On change</a></li><li class="leaf"><a href="/brandaris/docs/interaction/slider/">Slider</a></li><li class="leaf"><a href="/brandaris/docs/interaction/tabs/">Tabs</a></li><li class="leaf"><a href="/brandaris/docs/interaction/toggle/">Toggle</a></li>
    </ul></li><li class="branch  open"><a href="/brandaris/docs/layout/">Layout</a><ul class="menu">
        <li class="leaf"><a href="/brandaris/docs/layout/navigation/">Navigation</a></li>
    </ul></li><li class="leaf"><a href="/brandaris/docs/pages/">Pages</a></li><li class="leaf"><a href="/brandaris/docs/themes/">Themes</a></li>
    </ul>
        </nav>
      </div>
    </div>
  </main>

  
<div class="mt-2 bg-gray-light">
  <footer class="container">
    <div class="row center-xs">
      <div class="col-xs">
        <p class="small align-center">All code on this site is licensed under the <a href="https://opensource.org/licenses/MIT">MIT License</a> unless otherwise stated. &copy; 2019 Maarten Brakkee. This site is automatically build with <span class="clr-red">&hearts;</span> using <a href="https://jekyllrb.com/">Jekyll</a> and <a href="https://webpack.js.org/">Webpack</a>. View this project on <a href="https://bitbucket.org/maartenbrakkee/brandaris/overview">Bitbucket</a>.</p>
      </div>
    </div>
  </footer>
</div>


<div id="outdated"></div>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-84039417-1', 'auto');
  ga('send', 'pageview');
</script>

<script src="/brandaris/assets/scripts/brandaris.docs.min.js"></script>
<script src="/brandaris/assets/scripts/brandaris.min.js"></script>
<script src="/brandaris/assets/scripts/brandaris.charts.min.js"></script>

</body>

</html>
