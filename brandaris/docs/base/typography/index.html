<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
<meta name="viewport" content="width=device-width">
<meta name="description" content="Brandaris, a strong and sleek CSS-only framework.">
<meta name="author" content="Maarten Brakkee">

<title>Typography · Brandaris</title>

<!-- Brandaris core CSS -->
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Inconsolata|Lato|Noto+Serif:400,400i,700&display=swap">
<link href="/brandaris/assets/styles/brandaris.min.css" rel="stylesheet">
<link href="/brandaris/assets/styles/brandaris.docs.min.css" rel="stylesheet">

<!-- Favicons -->
<link rel="apple-touch-icon" href="/brandaris/assets/images/apple-touch-icon.png">
<link rel="icon" href="/brandaris/favicon.ico">

<!-- Brandaris Open Graph -->
<meta property="og:locale" content="en_UK">
<meta property="og:type" content="website">
<meta property="og:site_name" content="Brandaris: CSS-only framework">
<meta property="og:title" content="Typography &middot; Brandaris">
<meta property="og:description" content="Brandaris, a strong and sleek CSS-only framework.">
<meta property="og:url" content="http://maartenbrakkee.bitbucket.org/Brandaris/">
<meta property="og:image" content="http://maartenbrakkee.bitbucket.org/Brandaris/assets/images/repo.jpg">

<script src="/brandaris/assets/scripts/brandaris.header.min.js" async></script>

</head>

<body>
  <div class="bg-blue py-2 mb-2">
    <div class="container">
      <div class="row middle">
        <div class="col-md-9 clr-white">
          
          <h1>Base</h1>
          <p class="lead">Global styles, such as typography, colors, grid, etc.</p>
          
        </div>
        <div class="col-md-3">
          <div id="search-container" class="search">
            <input type="text" id="search-input" placeholder="search..." aria-label="Search">
            <div class="results hide">
              <ul id="results-container"></ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <main class="content container">
    <div class="row">
      <div class="col-md-9 col-lg-8 last-lg">
        <h2>Typography</h2>
        <p>Brandaris uses modular scale, vertical rhythm, and responsive ratio based headlines for typography styling. By default it has a <code class="highlighter-rouge">8px</code> baseline. Furthermore, the following utilities can be used to add additional styles to texts.</p>

<h3 id="headings">Headings</h3>

<p>All HTML headings, <code class="highlighter-rouge">&lt;h1&gt;</code> through <code class="highlighter-rouge">&lt;h6&gt;</code>, are available.</p>

<div class="docs-example" data-example-id="">
<h1>h1. Brandaris heading</h1>
<h2>h2. Brandaris heading</h2>
<h3>h3. Brandaris heading</h3>
<h4>h4. Brandaris heading</h4>
<h5>h5. Brandaris heading</h5>
<h6>h6. Brandaris heading</h6>
</div>
<div class="highlight"><pre><code class="language-html" data-lang="html"><span class="nt">&lt;h1&gt;</span>h1. Brandaris heading<span class="nt">&lt;/h1&gt;</span>
<span class="nt">&lt;h2&gt;</span>h2. Brandaris heading<span class="nt">&lt;/h2&gt;</span>
<span class="nt">&lt;h3&gt;</span>h3. Brandaris heading<span class="nt">&lt;/h3&gt;</span>
<span class="nt">&lt;h4&gt;</span>h4. Brandaris heading<span class="nt">&lt;/h4&gt;</span>
<span class="nt">&lt;h5&gt;</span>h5. Brandaris heading<span class="nt">&lt;/h5&gt;</span>
<span class="nt">&lt;h6&gt;</span>h6. Brandaris heading<span class="nt">&lt;/h6&gt;</span></code></pre></div>

<p><code class="highlighter-rouge">.h1</code> through <code class="highlighter-rouge">.h6</code> classes are also available, for when you want to match the font styling of a heading but cannot use the associated HTML element.</p>

<figure class="highlight"><pre><code class="language-html" data-lang="html"><span class="nt">&lt;p</span> <span class="na">class=</span><span class="s">"h1"</span><span class="nt">&gt;</span>h1. Brandaris heading<span class="nt">&lt;/p&gt;</span>
<span class="nt">&lt;p</span> <span class="na">class=</span><span class="s">"h2"</span><span class="nt">&gt;</span>h2. Brandaris heading<span class="nt">&lt;/p&gt;</span>
<span class="nt">&lt;p</span> <span class="na">class=</span><span class="s">"h3"</span><span class="nt">&gt;</span>h3. Brandaris heading<span class="nt">&lt;/p&gt;</span>
<span class="nt">&lt;p</span> <span class="na">class=</span><span class="s">"h4"</span><span class="nt">&gt;</span>h4. Brandaris heading<span class="nt">&lt;/p&gt;</span>
<span class="nt">&lt;p</span> <span class="na">class=</span><span class="s">"h5"</span><span class="nt">&gt;</span>h5. Brandaris heading<span class="nt">&lt;/p&gt;</span>
<span class="nt">&lt;p</span> <span class="na">class=</span><span class="s">"h6"</span><span class="nt">&gt;</span>h6. Brandaris heading<span class="nt">&lt;/p&gt;</span></code></pre></figure>

<h4 id="customizing-headings">Customizing headings</h4>

<p>Use the included utility classes to recreate the small secondary heading text from Brandaris 3.</p>

<div class="docs-example" data-example-id="">
<h3>
  Fancy display heading
  <small class="clr-gray">With faded secondary text</small>
</h3>
</div>
<div class="highlight"><pre><code class="language-html" data-lang="html"><span class="nt">&lt;h3&gt;</span>
  Fancy display heading
  <span class="nt">&lt;small</span> <span class="na">class=</span><span class="s">"clr-gray"</span><span class="nt">&gt;</span>With faded secondary text<span class="nt">&lt;/small&gt;</span>
<span class="nt">&lt;/h3&gt;</span></code></pre></div>

<h3 id="lead">Lead</h3>

<p>Make a paragraph stand out by adding <code class="highlighter-rouge">.lead</code>.</p>

<div class="docs-example" data-example-id="">
<p class="lead">
  Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Duis mollis, est non commodo luctus.
</p>
</div>
<div class="highlight"><pre><code class="language-html" data-lang="html"><span class="nt">&lt;p</span> <span class="na">class=</span><span class="s">"lead"</span><span class="nt">&gt;</span>
  Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Duis mollis, est non commodo luctus.
<span class="nt">&lt;/p&gt;</span></code></pre></div>

<h3 id="font-families">Font families</h3>

<div class="docs-example" data-example-id="">
<span>Visualize text inside your element with three different font families:</span>
<ul>
  <li><span class="font-sans">sans serif</span></li>
  <li><span class="font-serif">serif</span></li>
  <li><span class="font-mono">monospace</span></li>
</ul>
</div>
<div class="highlight"><pre><code class="language-html" data-lang="html"><span class="nt">&lt;span&gt;</span>Visualize text inside your element with three different font families:<span class="nt">&lt;/span&gt;</span>
<span class="nt">&lt;ul&gt;</span>
  <span class="nt">&lt;li&gt;&lt;span</span> <span class="na">class=</span><span class="s">"font-sans"</span><span class="nt">&gt;</span>sans serif<span class="nt">&lt;/span&gt;&lt;/li&gt;</span>
  <span class="nt">&lt;li&gt;&lt;span</span> <span class="na">class=</span><span class="s">"font-serif"</span><span class="nt">&gt;</span>serif<span class="nt">&lt;/span&gt;&lt;/li&gt;</span>
  <span class="nt">&lt;li&gt;&lt;span</span> <span class="na">class=</span><span class="s">"font-mono"</span><span class="nt">&gt;</span>monospace<span class="nt">&lt;/span&gt;&lt;/li&gt;</span>
<span class="nt">&lt;/ul&gt;</span></code></pre></div>

<h3 id="inline-text-elements">Inline text elements</h3>

<p>Styling for common inline HTML5 elements.</p>

<div class="docs-example" data-example-id="">
<p>You can use the mark tag to <mark>highlight</mark> text.</p>
<p><del>This line of text is meant to be treated as deleted text.</del></p>
<p><s>This line of text is meant to be treated as no longer accurate.</s></p>
<p><ins>This line of text is meant to be treated as an addition to the document.</ins></p>
<p><u>This line of text will render as underlined</u></p>
<p><small>This line of text is meant to be treated as fine print.</small></p>
<p><strong>This line rendered as bold text.</strong></p>
<p><em>This line rendered as italicized text.</em></p>
<p><kbd>Ctrl</kbd> + <kbd>Z</kbd></p>
<p>This is a sentence with sup<sup>this</sup> and sub<sub>that</sub>.</p>
<p>Financial data with tabular nums <span class="tabular-nums">01.234.567,89</span> and without 01.234.567,89.</p>
</div>
<div class="highlight"><pre><code class="language-html" data-lang="html"><span class="nt">&lt;p&gt;</span>You can use the mark tag to <span class="nt">&lt;mark&gt;</span>highlight<span class="nt">&lt;/mark&gt;</span> text.<span class="nt">&lt;/p&gt;</span>
<span class="nt">&lt;p&gt;&lt;del&gt;</span>This line of text is meant to be treated as deleted text.<span class="nt">&lt;/del&gt;&lt;/p&gt;</span>
<span class="nt">&lt;p&gt;&lt;s&gt;</span>This line of text is meant to be treated as no longer accurate.<span class="nt">&lt;/s&gt;&lt;/p&gt;</span>
<span class="nt">&lt;p&gt;&lt;ins&gt;</span>This line of text is meant to be treated as an addition to the document.<span class="nt">&lt;/ins&gt;&lt;/p&gt;</span>
<span class="nt">&lt;p&gt;&lt;u&gt;</span>This line of text will render as underlined<span class="nt">&lt;/u&gt;&lt;/p&gt;</span>
<span class="nt">&lt;p&gt;&lt;small&gt;</span>This line of text is meant to be treated as fine print.<span class="nt">&lt;/small&gt;&lt;/p&gt;</span>
<span class="nt">&lt;p&gt;&lt;strong&gt;</span>This line rendered as bold text.<span class="nt">&lt;/strong&gt;&lt;/p&gt;</span>
<span class="nt">&lt;p&gt;&lt;em&gt;</span>This line rendered as italicized text.<span class="nt">&lt;/em&gt;&lt;/p&gt;</span>
<span class="nt">&lt;p&gt;&lt;kbd&gt;</span>Ctrl<span class="nt">&lt;/kbd&gt;</span> + <span class="nt">&lt;kbd&gt;</span>Z<span class="nt">&lt;/kbd&gt;&lt;/p&gt;</span>
<span class="nt">&lt;p&gt;</span>This is a sentence with sup<span class="nt">&lt;sup&gt;</span>this<span class="nt">&lt;/sup&gt;</span> and sub<span class="nt">&lt;sub&gt;</span>that<span class="nt">&lt;/sub&gt;</span>.<span class="nt">&lt;/p&gt;</span>
<span class="nt">&lt;p&gt;</span>Financial data with tabular nums <span class="nt">&lt;span</span> <span class="na">class=</span><span class="s">"tabular-nums"</span><span class="nt">&gt;</span>01.234.567,89<span class="nt">&lt;/span&gt;</span> and without 01.234.567,89.<span class="nt">&lt;/p&gt;</span></code></pre></div>

<p><code class="highlighter-rouge">.mark</code> and <code class="highlighter-rouge">.small</code> classes are also available to apply the same styles as <code class="highlighter-rouge">&lt;mark&gt;</code> and <code class="highlighter-rouge">&lt;small&gt;</code> while avoiding any unwanted semantic implications that the tags would bring.</p>

<p>While not shown above, feel free to use <code class="highlighter-rouge">&lt;b&gt;</code> and <code class="highlighter-rouge">&lt;i&gt;</code> in HTML5. <code class="highlighter-rouge">&lt;b&gt;</code> is meant to highlight words or phrases without conveying additional importance while <code class="highlighter-rouge">&lt;i&gt;</code> is mostly for voice, technical terms, etc.</p>

<h3 id="abbreviations">Abbreviations</h3>

<p>Stylized implementation of HTML’s <code class="highlighter-rouge">&lt;abbr&gt;</code> element for abbreviations and acronyms to show the expanded version on hover. Abbreviations with a <code class="highlighter-rouge">title</code> attribute have a light dotted bottom border and a help cursor on hover, providing additional context on hover and to users of assistive technologies.</p>

<p>Add <code class="highlighter-rouge">.initialism</code> to an abbreviation for a slightly smaller font-size.</p>

<div class="docs-example" data-example-id="">
<p><abbr title="attribute">attr</abbr></p>
<p><abbr title="HyperText Markup Language" class="initialism">HTML</abbr></p>
</div>
<div class="highlight"><pre><code class="language-html" data-lang="html"><span class="nt">&lt;p&gt;&lt;abbr</span> <span class="na">title=</span><span class="s">"attribute"</span><span class="nt">&gt;</span>attr<span class="nt">&lt;/abbr&gt;&lt;/p&gt;</span>
<span class="nt">&lt;p&gt;&lt;abbr</span> <span class="na">title=</span><span class="s">"HyperText Markup Language"</span> <span class="na">class=</span><span class="s">"initialism"</span><span class="nt">&gt;</span>HTML<span class="nt">&lt;/abbr&gt;&lt;/p&gt;</span></code></pre></div>

<h3 id="blockquotes">Blockquotes</h3>

<p>For quoting blocks of content from another source within your document. Wrap <code class="highlighter-rouge">&lt;blockquote class="blockquote"&gt;</code> around any <abbr title="HyperText Markup Language">HTML</abbr> as the quote.</p>

<div class="docs-example" data-example-id="">
<blockquote class="blockquote">
  <p class="m-b-0">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.</p>
</blockquote>
</div>
<div class="highlight"><pre><code class="language-html" data-lang="html"><span class="nt">&lt;blockquote</span> <span class="na">class=</span><span class="s">"blockquote"</span><span class="nt">&gt;</span>
  <span class="nt">&lt;p</span> <span class="na">class=</span><span class="s">"m-b-0"</span><span class="nt">&gt;</span>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.<span class="nt">&lt;/p&gt;</span>
<span class="nt">&lt;/blockquote&gt;</span></code></pre></div>

<h4 id="naming-a-source">Naming a source</h4>

<p>Add a <code class="highlighter-rouge">&lt;footer class="blockquote-footer"&gt;</code> for identifying the source. Wrap the name of the source work in <code class="highlighter-rouge">&lt;cite&gt;</code>.</p>

<div class="docs-example" data-example-id="">
<blockquote class="blockquote">
  <p class="m-b-0">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.</p>
  <footer class="blockquote-footer">Someone famous in <cite title="Source Title">Source Title</cite></footer>
</blockquote>
</div>
<div class="highlight"><pre><code class="language-html" data-lang="html"><span class="nt">&lt;blockquote</span> <span class="na">class=</span><span class="s">"blockquote"</span><span class="nt">&gt;</span>
  <span class="nt">&lt;p</span> <span class="na">class=</span><span class="s">"m-b-0"</span><span class="nt">&gt;</span>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.<span class="nt">&lt;/p&gt;</span>
  <span class="nt">&lt;footer</span> <span class="na">class=</span><span class="s">"blockquote-footer"</span><span class="nt">&gt;</span>Someone famous in <span class="nt">&lt;cite</span> <span class="na">title=</span><span class="s">"Source Title"</span><span class="nt">&gt;</span>Source Title<span class="nt">&lt;/cite&gt;&lt;/footer&gt;</span>
<span class="nt">&lt;/blockquote&gt;</span></code></pre></div>

<h4 id="reverse-layout">Reverse layout</h4>

<p>Add <code class="highlighter-rouge">.blockquote-reverse</code> for a blockquote with right-aligned content.</p>

<div class="docs-example" data-example-id="">
<blockquote class="blockquote blockquote-reverse">
  <p class="m-b-0">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.</p>
  <footer class="blockquote-footer">Someone famous in <cite title="Source Title">Source Title</cite></footer>
</blockquote>
</div>
<div class="highlight"><pre><code class="language-html" data-lang="html"><span class="nt">&lt;blockquote</span> <span class="na">class=</span><span class="s">"blockquote blockquote-reverse"</span><span class="nt">&gt;</span>
  <span class="nt">&lt;p</span> <span class="na">class=</span><span class="s">"m-b-0"</span><span class="nt">&gt;</span>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.<span class="nt">&lt;/p&gt;</span>
  <span class="nt">&lt;footer</span> <span class="na">class=</span><span class="s">"blockquote-footer"</span><span class="nt">&gt;</span>Someone famous in <span class="nt">&lt;cite</span> <span class="na">title=</span><span class="s">"Source Title"</span><span class="nt">&gt;</span>Source Title<span class="nt">&lt;/cite&gt;&lt;/footer&gt;</span>
<span class="nt">&lt;/blockquote&gt;</span></code></pre></div>

<h3 id="lists">Lists</h3>

<h4 id="unstyled">Unstyled</h4>

<p>Remove the default <code class="highlighter-rouge">list-style</code> and left margin on list items (immediate children only). <strong>This only applies to immediate children list items</strong>, meaning you will need to add the class for any nested lists as well.</p>

<div class="docs-example" data-example-id="">
<ul class="list-none">
  <li>Lorem ipsum dolor sit amet</li>
  <li>Consectetur adipiscing elit</li>
  <li>Integer molestie lorem at massa</li>
  <li>Facilisis in pretium nisl aliquet</li>
  <li>Nulla volutpat aliquam velit
    <ul>
      <li>Phasellus iaculis neque</li>
      <li>Purus sodales ultricies</li>
      <li>Vestibulum laoreet porttitor sem</li>
      <li>Ac tristique libero volutpat at</li>
    </ul>
  </li>
  <li>Faucibus porta lacus fringilla vel</li>
  <li>Aenean sit amet erat nunc</li>
  <li>Eget porttitor lorem</li>
</ul>
</div>
<div class="highlight"><pre><code class="language-html" data-lang="html"><span class="nt">&lt;ul</span> <span class="na">class=</span><span class="s">"list-none"</span><span class="nt">&gt;</span>
  <span class="nt">&lt;li&gt;</span>Lorem ipsum dolor sit amet<span class="nt">&lt;/li&gt;</span>
  <span class="nt">&lt;li&gt;</span>Consectetur adipiscing elit<span class="nt">&lt;/li&gt;</span>
  <span class="nt">&lt;li&gt;</span>Integer molestie lorem at massa<span class="nt">&lt;/li&gt;</span>
  <span class="nt">&lt;li&gt;</span>Facilisis in pretium nisl aliquet<span class="nt">&lt;/li&gt;</span>
  <span class="nt">&lt;li&gt;</span>Nulla volutpat aliquam velit
    <span class="nt">&lt;ul&gt;</span>
      <span class="nt">&lt;li&gt;</span>Phasellus iaculis neque<span class="nt">&lt;/li&gt;</span>
      <span class="nt">&lt;li&gt;</span>Purus sodales ultricies<span class="nt">&lt;/li&gt;</span>
      <span class="nt">&lt;li&gt;</span>Vestibulum laoreet porttitor sem<span class="nt">&lt;/li&gt;</span>
      <span class="nt">&lt;li&gt;</span>Ac tristique libero volutpat at<span class="nt">&lt;/li&gt;</span>
    <span class="nt">&lt;/ul&gt;</span>
  <span class="nt">&lt;/li&gt;</span>
  <span class="nt">&lt;li&gt;</span>Faucibus porta lacus fringilla vel<span class="nt">&lt;/li&gt;</span>
  <span class="nt">&lt;li&gt;</span>Aenean sit amet erat nunc<span class="nt">&lt;/li&gt;</span>
  <span class="nt">&lt;li&gt;</span>Eget porttitor lorem<span class="nt">&lt;/li&gt;</span>
<span class="nt">&lt;/ul&gt;</span></code></pre></div>

<h4 id="inline">Inline</h4>

<p>Remove a list’s bullets and apply some light <code class="highlighter-rouge">margin</code> with a combination of two classes, <code class="highlighter-rouge">.list-inline</code>.</p>

<div class="docs-example" data-example-id="">
<ul class="list-inline">
  <li class="list-inline-item">Lorem ipsum</li>
  <li class="list-inline-item">Phasellus iaculis</li>
  <li class="list-inline-item">Nulla volutpat</li>
</ul>
</div>
<div class="highlight"><pre><code class="language-html" data-lang="html"><span class="nt">&lt;ul</span> <span class="na">class=</span><span class="s">"list-inline"</span><span class="nt">&gt;</span>
  <span class="nt">&lt;li</span> <span class="na">class=</span><span class="s">"list-inline-item"</span><span class="nt">&gt;</span>Lorem ipsum<span class="nt">&lt;/li&gt;</span>
  <span class="nt">&lt;li</span> <span class="na">class=</span><span class="s">"list-inline-item"</span><span class="nt">&gt;</span>Phasellus iaculis<span class="nt">&lt;/li&gt;</span>
  <span class="nt">&lt;li</span> <span class="na">class=</span><span class="s">"list-inline-item"</span><span class="nt">&gt;</span>Nulla volutpat<span class="nt">&lt;/li&gt;</span>
<span class="nt">&lt;/ul&gt;</span></code></pre></div>

<h4 id="description-list-alignment">Description list alignment</h4>

<p>Align terms and descriptions horizontally by using our grid system’s predefined classes (or semantic mixins). For longer terms, you can optionally add a <code class="highlighter-rouge">.text-truncate</code> class to truncate the text with an ellipsis.</p>

<div class="docs-example" data-example-id="">
<dl class="row">
  <dt class="col-sm-3">Description lists</dt>
  <dd class="col-sm-9">A description list is perfect for defining terms.</dd>

  <dt class="col-sm-3">Euismod</dt>
  <dd class="col-sm-9">Vestibulum id ligula porta felis euismod semper eget lacinia odio sem nec elit.</dd>
  <dd class="col-sm-9 col-sm-offset-3">Donec id elit non mi porta gravida at eget metus.</dd>

  <dt class="col-sm-3">Malesuada porta</dt>
  <dd class="col-sm-9">Etiam porta sem malesuada magna mollis euismod.</dd>

  <dt class="col-sm-3 text-truncate">Truncated term is truncated</dt>
  <dd class="col-sm-9">Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.</dd>

  <dt class="col-sm-3">Nesting</dt>
  <dd class="col-sm-9">
    <dl class="row">
      <dt class="col-sm-4">Nested definition list</dt>
      <dd class="col-sm-8">Aenean posuere, tortor sed cursus feugiat, nunc augue blandit nunc.</dd>
    </dl>
  </dd>
</dl>
</div>
<div class="highlight"><pre><code class="language-html" data-lang="html"><span class="nt">&lt;dl</span> <span class="na">class=</span><span class="s">"row"</span><span class="nt">&gt;</span>
  <span class="nt">&lt;dt</span> <span class="na">class=</span><span class="s">"col-sm-3"</span><span class="nt">&gt;</span>Description lists<span class="nt">&lt;/dt&gt;</span>
  <span class="nt">&lt;dd</span> <span class="na">class=</span><span class="s">"col-sm-9"</span><span class="nt">&gt;</span>A description list is perfect for defining terms.<span class="nt">&lt;/dd&gt;</span>

  <span class="nt">&lt;dt</span> <span class="na">class=</span><span class="s">"col-sm-3"</span><span class="nt">&gt;</span>Euismod<span class="nt">&lt;/dt&gt;</span>
  <span class="nt">&lt;dd</span> <span class="na">class=</span><span class="s">"col-sm-9"</span><span class="nt">&gt;</span>Vestibulum id ligula porta felis euismod semper eget lacinia odio sem nec elit.<span class="nt">&lt;/dd&gt;</span>
  <span class="nt">&lt;dd</span> <span class="na">class=</span><span class="s">"col-sm-9 col-sm-offset-3"</span><span class="nt">&gt;</span>Donec id elit non mi porta gravida at eget metus.<span class="nt">&lt;/dd&gt;</span>

  <span class="nt">&lt;dt</span> <span class="na">class=</span><span class="s">"col-sm-3"</span><span class="nt">&gt;</span>Malesuada porta<span class="nt">&lt;/dt&gt;</span>
  <span class="nt">&lt;dd</span> <span class="na">class=</span><span class="s">"col-sm-9"</span><span class="nt">&gt;</span>Etiam porta sem malesuada magna mollis euismod.<span class="nt">&lt;/dd&gt;</span>

  <span class="nt">&lt;dt</span> <span class="na">class=</span><span class="s">"col-sm-3 text-truncate"</span><span class="nt">&gt;</span>Truncated term is truncated<span class="nt">&lt;/dt&gt;</span>
  <span class="nt">&lt;dd</span> <span class="na">class=</span><span class="s">"col-sm-9"</span><span class="nt">&gt;</span>Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.<span class="nt">&lt;/dd&gt;</span>

  <span class="nt">&lt;dt</span> <span class="na">class=</span><span class="s">"col-sm-3"</span><span class="nt">&gt;</span>Nesting<span class="nt">&lt;/dt&gt;</span>
  <span class="nt">&lt;dd</span> <span class="na">class=</span><span class="s">"col-sm-9"</span><span class="nt">&gt;</span>
    <span class="nt">&lt;dl</span> <span class="na">class=</span><span class="s">"row"</span><span class="nt">&gt;</span>
      <span class="nt">&lt;dt</span> <span class="na">class=</span><span class="s">"col-sm-4"</span><span class="nt">&gt;</span>Nested definition list<span class="nt">&lt;/dt&gt;</span>
      <span class="nt">&lt;dd</span> <span class="na">class=</span><span class="s">"col-sm-8"</span><span class="nt">&gt;</span>Aenean posuere, tortor sed cursus feugiat, nunc augue blandit nunc.<span class="nt">&lt;/dd&gt;</span>
    <span class="nt">&lt;/dl&gt;</span>
  <span class="nt">&lt;/dd&gt;</span>
<span class="nt">&lt;/dl&gt;</span></code></pre></div>

<h4 id="checklists">Checklists</h4>

<p>Use checklists, users will love them!</p>

<div class="docs-example" data-example-id="">
<ul class="list-check">
  <li>First check</li>
  <li>Second check</li>
  <li>Checkmark!</li>
</ul>
</div>
<div class="highlight"><pre><code class="language-html" data-lang="html"><span class="nt">&lt;ul</span> <span class="na">class=</span><span class="s">"list-check"</span><span class="nt">&gt;</span>
  <span class="nt">&lt;li&gt;</span>First check<span class="nt">&lt;/li&gt;</span>
  <span class="nt">&lt;li&gt;</span>Second check<span class="nt">&lt;/li&gt;</span>
  <span class="nt">&lt;li&gt;</span>Checkmark!<span class="nt">&lt;/li&gt;</span>
<span class="nt">&lt;/ul&gt;</span></code></pre></div>

<p>Also available in white:</p>

<div class="bg-blue p-3">
  <ul class="list-check list-check-white mb-0">
    <li>First check</li>
    <li>Second check</li>
    <li>Checkmark!</li>
  </ul>
</div>

<figure class="highlight my-2"><pre><code class="language-html" data-lang="html"><span class="nt">&lt;ul</span> <span class="na">class=</span><span class="s">"list-check list-check-white"</span><span class="nt">&gt;</span>
  <span class="nt">&lt;li&gt;</span>First check<span class="nt">&lt;/li&gt;</span>
  <span class="nt">&lt;li&gt;</span>Second check<span class="nt">&lt;/li&gt;</span>
  <span class="nt">&lt;li&gt;</span>Checkmark!<span class="nt">&lt;/li&gt;</span>
<span class="nt">&lt;/ul&gt;</span></code></pre></figure>

<h4 id="breadcrumb-lists">Breadcrumb lists</h4>

<p>Use breadcrumbs, they’re really nice!</p>

<div class="docs-example" data-example-id="">
<ul class="list-breadcrumb">
    <li><a href="#">Home</a></li>
    <li><a href="#">Second</a></li>
    <li>Third</li>
  </ul>
</div>
<div class="highlight my-2"><pre><code class="language-html" data-lang="html"><span class="nt">&lt;ul</span> <span class="na">class=</span><span class="s">"list-breadcrumb"</span><span class="nt">&gt;</span>
    <span class="nt">&lt;li&gt;&lt;a</span> <span class="na">href=</span><span class="s">"#"</span><span class="nt">&gt;</span>Home<span class="nt">&lt;/a&gt;&lt;/li&gt;</span>
    <span class="nt">&lt;li&gt;&lt;a</span> <span class="na">href=</span><span class="s">"#"</span><span class="nt">&gt;</span>Second<span class="nt">&lt;/a&gt;&lt;/li&gt;</span>
    <span class="nt">&lt;li&gt;</span>Third<span class="nt">&lt;/li&gt;</span>
  <span class="nt">&lt;/ul&gt;</span></code></pre></div>

<h4 id="text-alignment">Text alignment</h4>

<p>Use responsive alignment, it’s very useful.
As such, the classes are named using the format:</p>
<ul>
  <li><code class="highlighter-rouge">.align-{value}-{breakpoint}</code> for <code class="highlighter-rouge">sm</code>, <code class="highlighter-rouge">md</code>, <code class="highlighter-rouge">lg</code> and <code class="highlighter-rouge">xl</code>.</li>
</ul>

<p>Where <em>value</em> is one of:</p>
<ul>
  <li><code class="highlighter-rouge">left</code></li>
  <li><code class="highlighter-rouge">right</code></li>
  <li><code class="highlighter-rouge">center</code></li>
</ul>

<div class="docs-example" data-example-id="">
<p class="align-right-lg">Right aligned text on viewports sized LG or wider</p>
<p class="align-center-sm align-left-md">Center aligned text on viewports sized SM or wider</p>
<p class="align-right-md align-center-lg align-left-xl">Right aligned text on viewports MD, center aligned on LG and left aligned on xl.</p>
</div>
<div class="highlight my-3"><pre><code class="language-html" data-lang="html"><span class="nt">&lt;p</span> <span class="na">class=</span><span class="s">"align-right-lg"</span><span class="nt">&gt;</span>Right aligned text on viewports sized LG or wider<span class="nt">&lt;/p&gt;</span>
<span class="nt">&lt;p</span> <span class="na">class=</span><span class="s">"align-center-sm align-left-md"</span><span class="nt">&gt;</span>Center aligned text on viewports sized SM or wider<span class="nt">&lt;/p&gt;</span>
<span class="nt">&lt;p</span> <span class="na">class=</span><span class="s">"align-right-md align-center-lg align-left-xl"</span><span class="nt">&gt;</span>Right aligned text on viewports MD, center aligned on LG and left aligned on xl.<span class="nt">&lt;/p&gt;</span></code></pre></div>

      </div>
      <div class="col-md-3 col-lg-2 last-lg d-none d-md-block">
        <div class="tocbot-wrap">
          <span class="clr-gray">On this page</span>
          <div class="tocbot pt-2"></div>
        </div>
      </div>
      <div class="col-lg-2">
        <nav class="docs-nav">
          <ul class="menu">
        <li class="active  branch  open"><a href="/brandaris/docs/base/">Base</a><ul class="menu">
        <li class="leaf"><a href="/brandaris/docs/base/colors/">Colors</a></li><li class="leaf"><a href="/brandaris/docs/base/display/">Display</a></li><li class="leaf"><a href="/brandaris/docs/base/grid/">Grid</a></li><li class="leaf"><a href="/brandaris/docs/base/images/">Images</a></li><li class="leaf"><a href="/brandaris/docs/base/spacing/">Spacing</a></li><li class="selected  leaf"><a href="/brandaris/docs/base/typography/">Typography</a></li>
    </ul></li><li class="branch  open"><a href="/brandaris/docs/components/">Components</a><ul class="menu">
        <li class="leaf"><a href="/brandaris/docs/components/badge/">Badges and circles</a></li><li class="leaf"><a href="/brandaris/docs/components/box/">Boxes</a></li><li class="leaf"><a href="/brandaris/docs/components/buttons/">Buttons</a></li><li class="leaf"><a href="/brandaris/docs/components/charts/">Charts</a></li><li class="leaf"><a href="/brandaris/docs/components/dropdowns/">Dropdowns</a></li><li class="leaf"><a href="/brandaris/docs/components/forms/">Forms</a></li><li class="leaf"><a href="/brandaris/docs/components/icons/">Icons</a></li><li class="leaf"><a href="/brandaris/docs/components/notifications/">Notifications</a></li><li class="leaf"><a href="/brandaris/docs/components/spinner/">Spinner</a></li><li class="leaf"><a href="/brandaris/docs/components/tables/">Tables</a></li><li class="leaf"><a href="/brandaris/docs/components/triangles/">Triangles</a></li>
    </ul></li><li class="branch  open"><a href="/brandaris/docs/interaction/">Interaction</a><ul class="menu">
        <li class="leaf"><a href="/brandaris/docs/interaction/on-change/">On change</a></li><li class="leaf"><a href="/brandaris/docs/interaction/slider/">Slider</a></li><li class="leaf"><a href="/brandaris/docs/interaction/tabs/">Tabs</a></li><li class="leaf"><a href="/brandaris/docs/interaction/toggle/">Toggle</a></li>
    </ul></li><li class="branch  open"><a href="/brandaris/docs/layout/">Layout</a><ul class="menu">
        <li class="leaf"><a href="/brandaris/docs/layout/navigation/">Navigation</a></li>
    </ul></li><li class="leaf"><a href="/brandaris/docs/pages/">Pages</a></li><li class="leaf"><a href="/brandaris/docs/themes/">Themes</a></li>
    </ul>
        </nav>
      </div>
    </div>
  </main>

  
<div class="mt-2 bg-gray-light">
  <footer class="container">
    <div class="row center-xs">
      <div class="col-xs">
        <p class="small align-center">All code on this site is licensed under the <a href="https://opensource.org/licenses/MIT">MIT License</a> unless otherwise stated. &copy; 2019 Maarten Brakkee. This site is automatically build with <span class="clr-red">&hearts;</span> using <a href="https://jekyllrb.com/">Jekyll</a> and <a href="https://webpack.js.org/">Webpack</a>. View this project on <a href="https://bitbucket.org/maartenbrakkee/brandaris/overview">Bitbucket</a>.</p>
      </div>
    </div>
  </footer>
</div>


<div id="outdated"></div>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-84039417-1', 'auto');
  ga('send', 'pageview');
</script>

<script src="/brandaris/assets/scripts/brandaris.docs.min.js"></script>
<script src="/brandaris/assets/scripts/brandaris.min.js"></script>
<script src="/brandaris/assets/scripts/brandaris.charts.min.js"></script>

</body>

</html>
