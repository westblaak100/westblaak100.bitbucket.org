<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
<meta name="viewport" content="width=device-width">
<meta name="description" content="Brandaris, a strong and sleek CSS-only framework.">
<meta name="author" content="Maarten Brakkee">

<title>Grid · Brandaris</title>

<!-- Brandaris core CSS -->
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Inconsolata|Lato|Noto+Serif:400,400i,700&display=swap">
<link href="/brandaris/assets/styles/brandaris.min.css" rel="stylesheet">
<link href="/brandaris/assets/styles/brandaris.docs.min.css" rel="stylesheet">

<!-- Favicons -->
<link rel="apple-touch-icon" href="/brandaris/assets/images/apple-touch-icon.png">
<link rel="icon" href="/brandaris/favicon.ico">

<!-- Brandaris Open Graph -->
<meta property="og:locale" content="en_UK">
<meta property="og:type" content="website">
<meta property="og:site_name" content="Brandaris: CSS-only framework">
<meta property="og:title" content="Grid &middot; Brandaris">
<meta property="og:description" content="Brandaris, a strong and sleek CSS-only framework.">
<meta property="og:url" content="http://maartenbrakkee.bitbucket.org/Brandaris/">
<meta property="og:image" content="http://maartenbrakkee.bitbucket.org/Brandaris/assets/images/repo.jpg">

<script src="/brandaris/assets/scripts/brandaris.header.min.js" async></script>

</head>

<body>
  <div class="bg-blue py-2 mb-2">
    <div class="container">
      <div class="row middle">
        <div class="col-md-9 clr-white">
          
          <h1>Base</h1>
          <p class="lead">Global styles, such as typography, colors, grid, etc.</p>
          
        </div>
        <div class="col-md-3">
          <div id="search-container" class="search">
            <input type="text" id="search-input" placeholder="search..." aria-label="Search">
            <div class="results hide">
              <ul id="results-container"></ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <main class="content container">
    <div class="row">
      <div class="col-md-9 col-lg-8 last-lg">
        <h2>Grid</h2>
        <p>Brandaris includes a powerful mobile-first grid system for building layouts of all shapes and sizes. It’s based on a 12 column layout and has multiple tiers.</p>

<h3 id="how-it-works">How it works</h3>

<p>At a high level, here’s how the grid system works:</p>

<ul>
  <li>There are three major components: containers, rows, and columns.</li>
  <li>Containers—<code class="highlighter-rouge">.container</code> for fixed width or <code class="highlighter-rouge">.container-fluid</code> for full width—center your site’s contents and help align your grid content. You can use it in combination with <code class="highlighter-rouge">.container-full</code> for a conatiner without side padding.</li>
  <li>Rows are horizontal groups of columns that ensure your columns are lined up properly.</li>
  <li>Content should be placed within columns, and only columns may be immediate children of rows.</li>
  <li>Column classes indicate the number of columns you’d like to use out of the possible 12 per row.</li>
  <li>If you want three equal-width columns, you can easily use <code class="highlighter-rouge">.col-sm</code>.</li>
  <li>Column <code class="highlighter-rouge">width</code>s are set in percentages, so they’re always fluid and sized relative to their parent element.</li>
  <li>Columns have horizontal <code class="highlighter-rouge">padding</code> to create the gutters between individual columns.</li>
  <li>There are five grid tiers, one for each responsive breakpoint: extra small, small, medium, large, and extra large.</li>
  <li>Grid tiers are based on minimum widths, meaning they apply to that one tier and all those above it (e.g., <code class="highlighter-rouge">.col-sm-4</code> applies to small, medium, large, and extra large devices).</li>
</ul>

<h3 id="auto-layout-columns">Auto-layout columns</h3>
<p>Add any number of auto sizing columns to a row. Let the grid figure it out.</p>

<div class="row">
  <div class="col-lg-6">


<figure class="highlight"><pre><code class="language-html" data-lang="html"><span class="nt">&lt;div</span> <span class="na">class=</span><span class="s">"row"</span><span class="nt">&gt;</span>
  <span class="nt">&lt;div</span> <span class="na">class=</span><span class="s">"col-xs"</span><span class="nt">&gt;</span>
    One of three columns
  <span class="nt">&lt;/div&gt;</span>
  <span class="nt">&lt;div</span> <span class="na">class=</span><span class="s">"col-xs"</span><span class="nt">&gt;</span>
    One of three columns
  <span class="nt">&lt;/div&gt;</span>
  <span class="nt">&lt;div</span> <span class="na">class=</span><span class="s">"col-xs"</span><span class="nt">&gt;</span>
    One of three columns
  <span class="nt">&lt;/div&gt;</span>
<span class="nt">&lt;/div&gt;</span></code></pre></figure>


  </div>
  <div class="col-lg-6">
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 157 88.3">
      <rect x="10" y="10" class="clr-blue" width="42.8" height="68.3" />
      <rect x="57.1" y="10" class="clr-blue" width="42.8" height="68.3" />
      <rect x="104.2" y="10" class="clr-blue" width="42.8" height="68.3" />
    </svg>
  </div>
</div>

<h3 id="alignment">Alignment</h3>
<p>Add classes to align elements to the start or end of a row as well as the top, bottom, or center of a column.</p>

<h4 id="start">Start</h4>

<p>Align all columns to the start of the <code class="highlighter-rouge">.row</code>. <em>This is default.</em></p>

<div class="row">
  <div class="col-lg-6">


<figure class="highlight"><pre><code class="language-html" data-lang="html"><span class="nt">&lt;div</span> <span class="na">class=</span><span class="s">"row start-xs"</span><span class="nt">&gt;</span>
  <span class="nt">&lt;div</span> <span class="na">class=</span><span class="s">"col-4"</span><span class="nt">&gt;</span>
    ...
  <span class="nt">&lt;/div&gt;</span>
  <span class="nt">&lt;div</span> <span class="na">class=</span><span class="s">"col-4"</span><span class="nt">&gt;</span>
    ...
  <span class="nt">&lt;/div&gt;</span>
<span class="nt">&lt;/div&gt;</span></code></pre></figure>


  </div>
  <div class="col-lg-6">
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 157 70">
    <rect x="10" y="10" class="clr-blue" width="42.8" height="36.9" />
    <rect x="57.1" y="10" class="clr-blue" width="42.8" height="52.4" />
    </svg>
  </div>
</div>

<h4 id="center">Center</h4>

<p>Align all columns to the center of the <code class="highlighter-rouge">.row</code>.</p>

<div class="row">
  <div class="col-lg-6">


<figure class="highlight"><pre><code class="language-html" data-lang="html"><span class="nt">&lt;div</span> <span class="na">class=</span><span class="s">"row center-xs"</span><span class="nt">&gt;</span>
  <span class="nt">&lt;div</span> <span class="na">class=</span><span class="s">"col-4"</span><span class="nt">&gt;</span>
    ...
  <span class="nt">&lt;/div&gt;</span>
  <span class="nt">&lt;div</span> <span class="na">class=</span><span class="s">"col-4"</span><span class="nt">&gt;</span>
    ...
  <span class="nt">&lt;/div&gt;</span>
<span class="nt">&lt;/div&gt;</span></code></pre></figure>


  </div>
  <div class="col-lg-6">
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 157 70">
    <rect x="33.6" y="10" class="clr-blue" width="42.8" height="36.9" />
    <rect x="80.7" y="10" class="clr-blue" width="42.8" height="52.4" />
    </svg>
  </div>
</div>

<h4 id="end">End</h4>

<p>Align all columns to the end of the <code class="highlighter-rouge">.row</code>.</p>

<div class="row">
  <div class="col-lg-6">


<figure class="highlight"><pre><code class="language-html" data-lang="html"><span class="nt">&lt;div</span> <span class="na">class=</span><span class="s">"row end-xs"</span><span class="nt">&gt;</span>
  <span class="nt">&lt;div</span> <span class="na">class=</span><span class="s">"col-4"</span><span class="nt">&gt;</span>
    ...
  <span class="nt">&lt;/div&gt;</span>
  <span class="nt">&lt;div</span> <span class="na">class=</span><span class="s">"col-4"</span><span class="nt">&gt;</span>
    ...
  <span class="nt">&lt;/div&gt;</span>
<span class="nt">&lt;/div&gt;</span></code></pre></figure>


  </div>
  <div class="col-lg-6">
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 157 70">
    <rect x="57.1" y="10" class="clr-blue" width="42.8" height="36.9" />
    <rect x="104.2" y="10" class="clr-blue" width="42.8" height="52.4" />
    </svg>
  </div>
</div>

<p>You can also use different alignments for different viewport sizes:</p>

<figure class="highlight"><pre><code class="language-html" data-lang="html"><span class="nt">&lt;div</span> <span class="na">class=</span><span class="s">"row center-xs end-sm start-lg"</span><span class="nt">&gt;</span>
  <span class="nt">&lt;div</span> <span class="na">class=</span><span class="s">"col-xs-6"</span><span class="nt">&gt;</span>
    All together now
  <span class="nt">&lt;/div&gt;</span>
<span class="nt">&lt;/div&gt;</span></code></pre></figure>

<h4 id="top">Top</h4>

<p>Align all columns at the top of the <code class="highlighter-rouge">.row</code>. <em>This is default.</em></p>

<div class="row">
  <div class="col-lg-6">


<figure class="highlight"><pre><code class="language-html" data-lang="html"><span class="nt">&lt;div</span> <span class="na">class=</span><span class="s">"row top-xs"</span><span class="nt">&gt;</span>
  <span class="nt">&lt;div</span> <span class="na">class=</span><span class="s">"col-4"</span><span class="nt">&gt;</span>
    ...
  <span class="nt">&lt;/div&gt;</span>
  <span class="nt">&lt;div</span> <span class="na">class=</span><span class="s">"col-4"</span><span class="nt">&gt;</span>
    ...
  <span class="nt">&lt;/div&gt;</span>
  <span class="nt">&lt;div</span> <span class="na">class=</span><span class="s">"col-4"</span><span class="nt">&gt;</span>
    ...
  <span class="nt">&lt;/div&gt;</span>
<span class="nt">&lt;/div&gt;</span></code></pre></figure>


  </div>
  <div class="col-lg-6">
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 157 88.3">
      <rect x="10" y="10" class="clr-blue" width="42.8" height="36.9" />
      <rect x="57.1" y="10" class="clr-blue" width="42.8" height="68.3" />
      <rect x="104.2" y="10" class="clr-blue" width="42.8" height="52.4" />
    </svg>
  </div>
</div>

<h4 id="middle">Middle</h4>

<p>Align all columns in the middle of the <code class="highlighter-rouge">.row</code>.</p>

<div class="row">
  <div class="col-lg-6">


<figure class="highlight"><pre><code class="language-html" data-lang="html"><span class="nt">&lt;div</span> <span class="na">class=</span><span class="s">"row middle-xs"</span><span class="nt">&gt;</span>
  <span class="nt">&lt;div</span> <span class="na">class=</span><span class="s">"col-4"</span><span class="nt">&gt;</span>
    ...
  <span class="nt">&lt;/div&gt;</span>
  <span class="nt">&lt;div</span> <span class="na">class=</span><span class="s">"col-4"</span><span class="nt">&gt;</span>
    ...
  <span class="nt">&lt;/div&gt;</span>
  <span class="nt">&lt;div</span> <span class="na">class=</span><span class="s">"col-4"</span><span class="nt">&gt;</span>
    ...
  <span class="nt">&lt;/div&gt;</span>
<span class="nt">&lt;/div&gt;</span></code></pre></figure>


  </div>
  <div class="col-lg-6">
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 157 88.3">
      <rect x="10" y="26.4" class="clr-blue" width="42.8" height="36.9" />
      <rect x="57.1" y="10" class="clr-blue" width="42.8" height="68.3" />
      <rect x="104.2" y="18.7" class="clr-blue" width="42.8" height="52.4" />
    </svg>
  </div>
</div>

<h4 id="bottom">Bottom</h4>

<p>Align all columns to the bottom of the <code class="highlighter-rouge">.row</code>.</p>

<div class="row">
  <div class="col-lg-6">


<figure class="highlight"><pre><code class="language-html" data-lang="html"><span class="nt">&lt;div</span> <span class="na">class=</span><span class="s">"row bottom-xs"</span><span class="nt">&gt;</span>
  <span class="nt">&lt;div</span> <span class="na">class=</span><span class="s">"col-4"</span><span class="nt">&gt;</span>
    ...
  <span class="nt">&lt;/div&gt;</span>
  <span class="nt">&lt;div</span> <span class="na">class=</span><span class="s">"col-4"</span><span class="nt">&gt;</span>
    ...
  <span class="nt">&lt;/div&gt;</span>
  <span class="nt">&lt;div</span> <span class="na">class=</span><span class="s">"col-4"</span><span class="nt">&gt;</span>
    ...
  <span class="nt">&lt;/div&gt;</span>
<span class="nt">&lt;/div&gt;</span></code></pre></figure>


  </div>
  <div class="col-lg-6">
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 157 88.3">
      <rect x="10" y="41.3" class="clr-blue" width="42.8" height="36.9" />
      <rect x="57.1" y="10" class="clr-blue" width="42.8" height="68.3" />
      <rect x="104.2" y="25.8" class="clr-blue" width="42.8" height="52.4" />
    </svg>
  </div>
</div>

<h4 id="full">Full</h4>

<p>Stretch all columns for equal, full, height in the <code class="highlighter-rouge">.row</code>.</p>

<div class="row">
  <div class="col-lg-6">


<figure class="highlight"><pre><code class="language-html" data-lang="html"><span class="nt">&lt;div</span> <span class="na">class=</span><span class="s">"row full-xs"</span><span class="nt">&gt;</span>
  <span class="nt">&lt;div</span> <span class="na">class=</span><span class="s">"col-4"</span><span class="nt">&gt;</span>
    ...
  <span class="nt">&lt;/div&gt;</span>
  <span class="nt">&lt;div</span> <span class="na">class=</span><span class="s">"col-4"</span><span class="nt">&gt;</span>
    ...
  <span class="nt">&lt;/div&gt;</span>
  <span class="nt">&lt;div</span> <span class="na">class=</span><span class="s">"col-4"</span><span class="nt">&gt;</span>
    ...
  <span class="nt">&lt;/div&gt;</span>
<span class="nt">&lt;/div&gt;</span></code></pre></figure>


  </div>
  <div class="col-lg-6">
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 157 88.3">
      <rect x="10" y="10" class="clr-blue" width="42.8" height="68.3" />
      <rect x="57.1" y="10" class="clr-blue" width="42.8" height="68.3" />
      <rect x="104.2" y="10" class="clr-blue" width="42.8" height="68.3" />
    </svg>
  </div>
</div>

<h3 id="reordering">Reordering</h3>
<p>Add classes to reorder columns.</p>

<h4 id="first">First</h4>

<div class="row">
  <div class="col-lg-6">


<figure class="highlight"><pre><code class="language-html" data-lang="html"><span class="nt">&lt;div</span> <span class="na">class=</span><span class="s">"row"</span><span class="nt">&gt;</span>
  <span class="nt">&lt;div</span> <span class="na">class=</span><span class="s">"col-xs"</span><span class="nt">&gt;</span>
    1
  <span class="nt">&lt;/div&gt;</span>
  <span class="nt">&lt;div</span> <span class="na">class=</span><span class="s">"col-xs"</span><span class="nt">&gt;</span>
    2
  <span class="nt">&lt;/div&gt;</span>
  <span class="nt">&lt;div</span> <span class="na">class=</span><span class="s">"col-xs first-xs"</span><span class="nt">&gt;</span>
    3
  <span class="nt">&lt;/div&gt;</span>
<span class="nt">&lt;/div&gt;</span></code></pre></figure>


  </div>
  <div class="col-lg-6">
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 157 88.3">
      <rect x="10" y="10" class="clr-blue" width="42.8" height="36.9" />
      <rect x="57.1" y="10" class="clr-blue" width="42.8" height="68.3" />
      <rect x="104.2" y="10" class="clr-blue" width="42.8" height="52.4" />
      <text transform="matrix(1 0 0 1 26.5 32.616)" class="clr-white h6">3</text>
      <text transform="matrix(1 0 0 1 74.4 32.616)" class="clr-white h6">1</text>
      <text transform="matrix(1 0 0 1 121.2 32.616)" class="clr-white h6">2</text>
    </svg>
  </div>
</div>

<h4 id="last">Last</h4>

<div class="row">
  <div class="col-lg-6">


<figure class="highlight"><pre><code class="language-html" data-lang="html"><span class="nt">&lt;div</span> <span class="na">class=</span><span class="s">"row"</span><span class="nt">&gt;</span>
  <span class="nt">&lt;div</span> <span class="na">class=</span><span class="s">"col-xs last-xs"</span><span class="nt">&gt;</span>
    1
  <span class="nt">&lt;/div&gt;</span>
  <span class="nt">&lt;div</span> <span class="na">class=</span><span class="s">"col-xs"</span><span class="nt">&gt;</span>
    2
  <span class="nt">&lt;/div&gt;</span>
  <span class="nt">&lt;div</span> <span class="na">class=</span><span class="s">"col-xs"</span><span class="nt">&gt;</span>
    3
  <span class="nt">&lt;/div&gt;</span>
<span class="nt">&lt;/div&gt;</span></code></pre></figure>


  </div>
  <div class="col-lg-6">
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 157 88.3">
      <rect x="10" y="10" class="clr-blue" width="42.8" height="52.4" />
      <rect x="57.1" y="10" class="clr-blue" width="42.8" height="36.9" />
      <rect x="104.2" y="10" class="clr-blue" width="42.8" height="68.3" />
      <text transform="matrix(1 0 0 1 26.5 32.616)" class="clr-white h6">2</text>
      <text transform="matrix(1 0 0 1 74.4 32.616)" class="clr-white h6">3</text>
      <text transform="matrix(1 0 0 1 121.2 32.616)" class="clr-white h6">1</text>
    </svg>
  </div>
</div>

<h4 id="reverse">Reverse</h4>

<div class="row">
  <div class="col-lg-6">


<figure class="highlight"><pre><code class="language-html" data-lang="html"><span class="nt">&lt;div</span> <span class="na">class=</span><span class="s">"row reverse"</span><span class="nt">&gt;</span>
  <span class="nt">&lt;div</span> <span class="na">class=</span><span class="s">"col-xs"</span><span class="nt">&gt;</span>
    1
  <span class="nt">&lt;/div&gt;</span>
  <span class="nt">&lt;div</span> <span class="na">class=</span><span class="s">"col-xs"</span><span class="nt">&gt;</span>
    2
  <span class="nt">&lt;/div&gt;</span>
  <span class="nt">&lt;div</span> <span class="na">class=</span><span class="s">"col-xs"</span><span class="nt">&gt;</span>
    3
  <span class="nt">&lt;/div&gt;</span>
<span class="nt">&lt;/div&gt;</span></code></pre></figure>


  </div>
  <div class="col-lg-6">
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 157 88.3">
      <rect x="10" y="10" class="clr-blue" width="42.8" height="36.9" />
      <rect x="57.1" y="10" class="clr-blue" width="42.8" height="52.4" />
      <rect x="104.2" y="10" class="clr-blue" width="42.8" height="68.3" />
      <text transform="matrix(1 0 0 1 26.5 32.616)" class="clr-white h6">3</text>
      <text transform="matrix(1 0 0 1 74.4 32.616)" class="clr-white h6">2</text>
      <text transform="matrix(1 0 0 1 121.2 32.616)" class="clr-white h6">1</text>
    </svg>
  </div>
</div>

      </div>
      <div class="col-md-3 col-lg-2 last-lg d-none d-md-block">
        <div class="tocbot-wrap">
          <span class="clr-gray">On this page</span>
          <div class="tocbot pt-2"></div>
        </div>
      </div>
      <div class="col-lg-2">
        <nav class="docs-nav">
          <ul class="menu">
        <li class="active  branch  open"><a href="/brandaris/docs/base/">Base</a><ul class="menu">
        <li class="leaf"><a href="/brandaris/docs/base/colors/">Colors</a></li><li class="leaf"><a href="/brandaris/docs/base/display/">Display</a></li><li class="selected  leaf"><a href="/brandaris/docs/base/grid/">Grid</a></li><li class="leaf"><a href="/brandaris/docs/base/images/">Images</a></li><li class="leaf"><a href="/brandaris/docs/base/spacing/">Spacing</a></li><li class="leaf"><a href="/brandaris/docs/base/typography/">Typography</a></li>
    </ul></li><li class="branch  open"><a href="/brandaris/docs/components/">Components</a><ul class="menu">
        <li class="leaf"><a href="/brandaris/docs/components/badge/">Badges and circles</a></li><li class="leaf"><a href="/brandaris/docs/components/box/">Boxes</a></li><li class="leaf"><a href="/brandaris/docs/components/buttons/">Buttons</a></li><li class="leaf"><a href="/brandaris/docs/components/charts/">Charts</a></li><li class="leaf"><a href="/brandaris/docs/components/dropdowns/">Dropdowns</a></li><li class="leaf"><a href="/brandaris/docs/components/forms/">Forms</a></li><li class="leaf"><a href="/brandaris/docs/components/icons/">Icons</a></li><li class="leaf"><a href="/brandaris/docs/components/notifications/">Notifications</a></li><li class="leaf"><a href="/brandaris/docs/components/spinner/">Spinner</a></li><li class="leaf"><a href="/brandaris/docs/components/tables/">Tables</a></li><li class="leaf"><a href="/brandaris/docs/components/triangles/">Triangles</a></li>
    </ul></li><li class="branch  open"><a href="/brandaris/docs/interaction/">Interaction</a><ul class="menu">
        <li class="leaf"><a href="/brandaris/docs/interaction/on-change/">On change</a></li><li class="leaf"><a href="/brandaris/docs/interaction/slider/">Slider</a></li><li class="leaf"><a href="/brandaris/docs/interaction/tabs/">Tabs</a></li><li class="leaf"><a href="/brandaris/docs/interaction/toggle/">Toggle</a></li>
    </ul></li><li class="branch  open"><a href="/brandaris/docs/layout/">Layout</a><ul class="menu">
        <li class="leaf"><a href="/brandaris/docs/layout/navigation/">Navigation</a></li>
    </ul></li><li class="leaf"><a href="/brandaris/docs/pages/">Pages</a></li><li class="leaf"><a href="/brandaris/docs/themes/">Themes</a></li>
    </ul>
        </nav>
      </div>
    </div>
  </main>

  
<div class="mt-2 bg-gray-light">
  <footer class="container">
    <div class="row center-xs">
      <div class="col-xs">
        <p class="small align-center">All code on this site is licensed under the <a href="https://opensource.org/licenses/MIT">MIT License</a> unless otherwise stated. &copy; 2019 Maarten Brakkee. This site is automatically build with <span class="clr-red">&hearts;</span> using <a href="https://jekyllrb.com/">Jekyll</a> and <a href="https://webpack.js.org/">Webpack</a>. View this project on <a href="https://bitbucket.org/maartenbrakkee/brandaris/overview">Bitbucket</a>.</p>
      </div>
    </div>
  </footer>
</div>


<div id="outdated"></div>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-84039417-1', 'auto');
  ga('send', 'pageview');
</script>

<script src="/brandaris/assets/scripts/brandaris.docs.min.js"></script>
<script src="/brandaris/assets/scripts/brandaris.min.js"></script>
<script src="/brandaris/assets/scripts/brandaris.charts.min.js"></script>

</body>

</html>
