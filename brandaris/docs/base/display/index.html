<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
<meta name="viewport" content="width=device-width">
<meta name="description" content="Brandaris, a strong and sleek CSS-only framework.">
<meta name="author" content="Maarten Brakkee">

<title>Display · Brandaris</title>

<!-- Brandaris core CSS -->
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Inconsolata|Lato|Noto+Serif:400,400i,700&display=swap">
<link href="/brandaris/assets/styles/brandaris.min.css" rel="stylesheet">
<link href="/brandaris/assets/styles/brandaris.docs.min.css" rel="stylesheet">

<!-- Favicons -->
<link rel="apple-touch-icon" href="/brandaris/assets/images/apple-touch-icon.png">
<link rel="icon" href="/brandaris/favicon.ico">

<!-- Brandaris Open Graph -->
<meta property="og:locale" content="en_UK">
<meta property="og:type" content="website">
<meta property="og:site_name" content="Brandaris: CSS-only framework">
<meta property="og:title" content="Display &middot; Brandaris">
<meta property="og:description" content="Brandaris, a strong and sleek CSS-only framework.">
<meta property="og:url" content="http://maartenbrakkee.bitbucket.org/Brandaris/">
<meta property="og:image" content="http://maartenbrakkee.bitbucket.org/Brandaris/assets/images/repo.jpg">

<script src="/brandaris/assets/scripts/brandaris.header.min.js" async></script>

</head>

<body>
  <div class="bg-blue py-2 mb-2">
    <div class="container">
      <div class="row middle">
        <div class="col-md-9 clr-white">
          
          <h1>Base</h1>
          <p class="lead">Global styles, such as typography, colors, grid, etc.</p>
          
        </div>
        <div class="col-md-3">
          <div id="search-container" class="search">
            <input type="text" id="search-input" placeholder="search..." aria-label="Search">
            <div class="results hide">
              <ul id="results-container"></ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <main class="content container">
    <div class="row">
      <div class="col-md-9 col-lg-8 last-lg">
        <h2>Display</h2>
        <h3 id="how-it-works">How it works</h3>

<p>Change the value of the <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/display"><code class="highlighter-rouge">display</code> property</a> with our responsive display utility classes. We purposely support only a subset of all possible values for <code class="highlighter-rouge">display</code>. Classes can be combined for various effects as you need.</p>

<h3 id="notation">Notation</h3>

<p>Display utility classes that apply to all breakpoints, from <code class="highlighter-rouge">xs</code> to <code class="highlighter-rouge">xl</code>, have no breakpoint abbreviation in them. This is because those classes are applied from <code class="highlighter-rouge">min-width: 0;</code> and up, and thus are not bound by a media query. The remaining breakpoints, however, do include a breakpoint abbreviation.</p>

<p>As such, the classes are named using the format:</p>

<ul>
  <li><code class="highlighter-rouge">.d-{value}</code> for <code class="highlighter-rouge">xs</code></li>
  <li><code class="highlighter-rouge">.d-{breakpoint}-{value}</code> for <code class="highlighter-rouge">sm</code>, <code class="highlighter-rouge">md</code>, <code class="highlighter-rouge">lg</code>, and <code class="highlighter-rouge">xl</code>.</li>
</ul>

<p>Where <em>value</em> is one of:</p>

<ul>
  <li><code class="highlighter-rouge">none</code></li>
  <li><code class="highlighter-rouge">inline</code></li>
  <li><code class="highlighter-rouge">inline-block</code></li>
  <li><code class="highlighter-rouge">block</code></li>
  <li><code class="highlighter-rouge">flex</code></li>
  <li><code class="highlighter-rouge">inline-flex</code></li>
</ul>

<p>The media queries effect screen widths with the given breakpoint <em>or larger</em>. For example, <code class="highlighter-rouge">.d-lg-none</code> sets <code class="highlighter-rouge">display: none;</code> on both <code class="highlighter-rouge">lg</code> and <code class="highlighter-rouge">xl</code> screens.</p>

<h3 id="examples">Examples</h3>

<figure class="highlight"><pre><code class="language-html" data-lang="html"><span class="nt">&lt;div</span> <span class="na">class=</span><span class="s">"d-inline p-2 bg-blue text-white"</span><span class="nt">&gt;</span>d-inline<span class="nt">&lt;/div&gt;</span>
<span class="nt">&lt;div</span> <span class="na">class=</span><span class="s">"d-inline p-2 bg-dark text-white"</span><span class="nt">&gt;</span>d-inline<span class="nt">&lt;/div&gt;</span></code></pre></figure>

<figure class="highlight"><pre><code class="language-html" data-lang="html"><span class="nt">&lt;span</span> <span class="na">class=</span><span class="s">"d-block p-2 bg-blue text-white"</span><span class="nt">&gt;</span>d-block<span class="nt">&lt;/span&gt;</span>
<span class="nt">&lt;span</span> <span class="na">class=</span><span class="s">"d-block p-2 bg-dark text-white"</span><span class="nt">&gt;</span>d-block<span class="nt">&lt;/span&gt;</span></code></pre></figure>

<h3 id="hiding-elements">Hiding elements</h3>

<p>For faster mobile-friendly development, use responsive display classes for showing and hiding elements by device. Avoid creating entirely different versions of the same site, instead hide element responsively for each screen size.</p>

<p>To hide elements simply use the <code class="highlighter-rouge">.d-none</code> class or one of the <code class="highlighter-rouge">.d-{sm,md,lg,xl}-none</code> classes for any responsive screen variation.</p>

<p>To show an element only on a given interval of screen sizes you can combine one <code class="highlighter-rouge">.d-*-none</code> class with a <code class="highlighter-rouge">.d-*-*</code> class, for example <code class="highlighter-rouge">.d-none .d-md-block .d-xl-none</code> will hide the element for all screen sizes except on medium and large devices.</p>

<table class="table table-full mb-2">
  <thead>
    <tr>
      <th>Screen Size</th>
      <th>Class</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>Hidden on all</td>
      <td><code class="highlighter-rouge">.d-none</code></td>
    </tr>
    <tr>
      <td>Hidden only on xs</td>
      <td><code class="highlighter-rouge">.d-none .d-sm-block</code></td>
    </tr>
    <tr>
      <td>Hidden only on sm</td>
      <td><code class="highlighter-rouge">.d-sm-none .d-md-block</code></td>
    </tr>
    <tr>
      <td>Hidden only on md</td>
      <td><code class="highlighter-rouge">.d-md-none .d-lg-block</code></td>
    </tr>
    <tr>
      <td>Hidden only on lg</td>
      <td><code class="highlighter-rouge">.d-lg-none .d-xl-block</code></td>
    </tr>
    <tr>
      <td>Hidden only on xl</td>
      <td><code class="highlighter-rouge">.d-xl-none</code></td>
    </tr>
    <tr>
      <td>Visible on all</td>
      <td><code class="highlighter-rouge">.d-block</code></td>
    </tr>
    <tr>
      <td>Visible only on xs</td>
      <td><code class="highlighter-rouge">.d-block .d-sm-none</code></td>
    </tr>
    <tr>
      <td>Visible only on sm</td>
      <td><code class="highlighter-rouge">.d-none .d-sm-block .d-md-none</code></td>
    </tr>
    <tr>
      <td>Visible only on md</td>
      <td><code class="highlighter-rouge">.d-none .d-md-block .d-lg-none</code></td>
    </tr>
    <tr>
      <td>Visible only on lg</td>
      <td><code class="highlighter-rouge">.d-none .d-lg-block .d-xl-none</code></td>
    </tr>
    <tr>
      <td>Visible only on xl</td>
      <td><code class="highlighter-rouge">.d-none .d-xl-block</code></td>
    </tr>
  </tbody>
</table>

<figure class="highlight"><pre><code class="language-html" data-lang="html"><span class="nt">&lt;div</span> <span class="na">class=</span><span class="s">"d-lg-none"</span><span class="nt">&gt;</span>hide on screens wider than lg<span class="nt">&lt;/div&gt;</span>
<span class="nt">&lt;div</span> <span class="na">class=</span><span class="s">"d-none d-lg-block"</span><span class="nt">&gt;</span>hide on screens smaller than lg<span class="nt">&lt;/div&gt;</span></code></pre></figure>

<h3 id="display-in-print">Display in print</h3>

<p>Change the <code class="highlighter-rouge">display</code> value of elements when printing with our print display utility classes. Includes support for the same <code class="highlighter-rouge">display</code> values as our responsive <code class="highlighter-rouge">.d-*</code> utilities.</p>

<ul>
  <li><code class="highlighter-rouge">.d-print-none</code></li>
  <li><code class="highlighter-rouge">.d-print-inline</code></li>
  <li><code class="highlighter-rouge">.d-print-inline-block</code></li>
  <li><code class="highlighter-rouge">.d-print-block</code></li>
  <li><code class="highlighter-rouge">.d-print-flex</code></li>
  <li><code class="highlighter-rouge">.d-print-inline-flex</code></li>
</ul>

<p>The print and display classes can be combined.</p>

<figure class="highlight"><pre><code class="language-html" data-lang="html"><span class="nt">&lt;div</span> <span class="na">class=</span><span class="s">"d-print-none"</span><span class="nt">&gt;</span>Screen Only (Hide on print only)<span class="nt">&lt;/div&gt;</span>
<span class="nt">&lt;div</span> <span class="na">class=</span><span class="s">"d-none d-print-block"</span><span class="nt">&gt;</span>Print Only (Hide on screen only)<span class="nt">&lt;/div&gt;</span>
<span class="nt">&lt;div</span> <span class="na">class=</span><span class="s">"d-none d-lg-block d-print-block"</span><span class="nt">&gt;</span>Hide up to large on screen, but always show on print<span class="nt">&lt;/div&gt;</span></code></pre></figure>


      </div>
      <div class="col-md-3 col-lg-2 last-lg d-none d-md-block">
        <div class="tocbot-wrap">
          <span class="clr-gray">On this page</span>
          <div class="tocbot pt-2"></div>
        </div>
      </div>
      <div class="col-lg-2">
        <nav class="docs-nav">
          <ul class="menu">
        <li class="active  branch  open"><a href="/brandaris/docs/base/">Base</a><ul class="menu">
        <li class="leaf"><a href="/brandaris/docs/base/colors/">Colors</a></li><li class="selected  leaf"><a href="/brandaris/docs/base/display/">Display</a></li><li class="leaf"><a href="/brandaris/docs/base/grid/">Grid</a></li><li class="leaf"><a href="/brandaris/docs/base/images/">Images</a></li><li class="leaf"><a href="/brandaris/docs/base/spacing/">Spacing</a></li><li class="leaf"><a href="/brandaris/docs/base/typography/">Typography</a></li>
    </ul></li><li class="branch  open"><a href="/brandaris/docs/components/">Components</a><ul class="menu">
        <li class="leaf"><a href="/brandaris/docs/components/badge/">Badges and circles</a></li><li class="leaf"><a href="/brandaris/docs/components/box/">Boxes</a></li><li class="leaf"><a href="/brandaris/docs/components/buttons/">Buttons</a></li><li class="leaf"><a href="/brandaris/docs/components/charts/">Charts</a></li><li class="leaf"><a href="/brandaris/docs/components/dropdowns/">Dropdowns</a></li><li class="leaf"><a href="/brandaris/docs/components/forms/">Forms</a></li><li class="leaf"><a href="/brandaris/docs/components/icons/">Icons</a></li><li class="leaf"><a href="/brandaris/docs/components/notifications/">Notifications</a></li><li class="leaf"><a href="/brandaris/docs/components/spinner/">Spinner</a></li><li class="leaf"><a href="/brandaris/docs/components/tables/">Tables</a></li><li class="leaf"><a href="/brandaris/docs/components/triangles/">Triangles</a></li>
    </ul></li><li class="branch  open"><a href="/brandaris/docs/interaction/">Interaction</a><ul class="menu">
        <li class="leaf"><a href="/brandaris/docs/interaction/on-change/">On change</a></li><li class="leaf"><a href="/brandaris/docs/interaction/slider/">Slider</a></li><li class="leaf"><a href="/brandaris/docs/interaction/tabs/">Tabs</a></li><li class="leaf"><a href="/brandaris/docs/interaction/toggle/">Toggle</a></li>
    </ul></li><li class="branch  open"><a href="/brandaris/docs/layout/">Layout</a><ul class="menu">
        <li class="leaf"><a href="/brandaris/docs/layout/navigation/">Navigation</a></li>
    </ul></li><li class="leaf"><a href="/brandaris/docs/pages/">Pages</a></li><li class="leaf"><a href="/brandaris/docs/themes/">Themes</a></li>
    </ul>
        </nav>
      </div>
    </div>
  </main>

  
<div class="mt-2 bg-gray-light">
  <footer class="container">
    <div class="row center-xs">
      <div class="col-xs">
        <p class="small align-center">All code on this site is licensed under the <a href="https://opensource.org/licenses/MIT">MIT License</a> unless otherwise stated. &copy; 2019 Maarten Brakkee. This site is automatically build with <span class="clr-red">&hearts;</span> using <a href="https://jekyllrb.com/">Jekyll</a> and <a href="https://webpack.js.org/">Webpack</a>. View this project on <a href="https://bitbucket.org/maartenbrakkee/brandaris/overview">Bitbucket</a>.</p>
      </div>
    </div>
  </footer>
</div>


<div id="outdated"></div>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-84039417-1', 'auto');
  ga('send', 'pageview');
</script>

<script src="/brandaris/assets/scripts/brandaris.docs.min.js"></script>
<script src="/brandaris/assets/scripts/brandaris.min.js"></script>
<script src="/brandaris/assets/scripts/brandaris.charts.min.js"></script>

</body>

</html>
